Rails.application.routes.draw do
  get 'product_groups/modify'
  get "product_groups/destroy"
  post "product_groups/getProducts" 
  post 'product_groups/removeMultipleGroups' 
  resources :product_groups

  get 'location_groups/modify'
  get "location_groups/destroy"
  post 'location_groups/getStores' 
  post 'location_groups/getStoresByCustAttr' 
  post 'location_groups/removeMultipleGroups' 
  resources :location_groups

  get "login/reset_password"
  get "login/index"
  post "login/create"
  get "login/logout"
 

  get 'worklist/index'
  get "users/modify"
  get "users/destroy"
  post "users/set_preference"
  post "users/get_location_hier"
  post "users/get_product_hier"
  resources :users

  get 'contract/new'
  get 'contract/create'
  post 'contract/getStores'
  post 'contract/getStoresByCustAttr'
  post 'contract/storeLocations'
  post 'contract/removeStore'
  post 'contract/storeLocationGroups'
  get 'contract/storeLocationGroups'
  post 'contract/createNewContract'
  get 'contract/modify'
  match 'contract/newFromScratch' => 'contract#newFromScratch' , via: [:get, :post]
  post 'contract/createNewContract'
  #post 'contract/newFromScratch'
  post 'contract/addNote'
  post 'contract/saveMarketingVehicle'
  post 'contract/priceVehicleProducts'
  post 'contract/getAllNote'
  post 'contract/getProducts'
  post 'contract/storeProducts'
  post 'contract/storeProductGroups'
  post 'contract/saveDate'
  post 'contract/getProductsGroupsHash'
  post 'contract/add_location_group'
  get 'contract/delete'
  post 'contract/create_tier'
  get 'contract/create_tier'
  #
  get 'contract/contractsMain'
  post 'contract/contractsMain'
  get 'contract/createTrial'
  post 'contract/createTierForVendor'
  post 'contract/update_tier'
  post 'contract/updatePriceVehical'
  post 'contract/breadcrumbCategory'
  post 'contract/breadcrumbBanner'
  get 'contract/modifyContractAttr'
  post 'contract/addContractAttribute'
  post 'contract/createVersion'
  
  
   get 'event_managements/destroy'
  post 'event_managements/addEvent'

  post 'pricing_guidelines/create_price_rules'
  post  'pricing_guidelines/get_price_rules'
  get 'pricing_guidelines/get_price_rules'
  post 'pricing_guidelines/create_price_grids'
  post  'pricing_guidelines/get_price_grids'
  get 'pricing_guidelines/get_price_grids'
  delete  'pricing_guidelines/destroy_price_rule'
  delete  'pricing_guidelines/destroy_price_grid'
  post  'pricing_guidelines/get_product_attribute'
  get 'pricing_guidelines/get_product_attribute'
  post  'pricing_guidelines/get_price_rules_attributes'
  get 'pricing_guidelines/get_price_rules_attributes'

  resources :pricing_guidelines


  namespace :api do
    get 'price_rules_services/addPriceRule'
  end

  namespace :api do
    get 'price_rules_services/updatePriceRule'
  end

  namespace :api do
    get 'price_rules_services/removePriceRule'
  end

  namespace :api do
    get 'price_rules_services/getAllPriceRulesForCategory'
  end

  namespace :api do
    get 'price_rules_services/getAllPriceRuleForProductIdAndLocationId'
  end

  namespace :api do
    get 'price_rules_services/getAllPriceRulesForCategoriesAndAttributes'
  end

  namespace :api do
    get 'price_rules_services/getAllPriceRulesForLocationsAndAttributes'
  end

  namespace :api do
    get 'price_grids_services/addPriceGrid'
  end

  namespace :api do
    get 'price_grids_services/updatePriceGrid'
  end

  namespace :api do
    get 'price_grids_services/removePriceGrid'
  end

  namespace :api do
    get 'price_grids_services/getPriceGridByCategories'
  end

  namespace :api do
    get 'price_grids_services/getPriceGridByLocation'
  end

  namespace :api do
    get 'price_grids_services/getPriceGridByCategoryAndLocation'
  end

  namespace :api do
    get 'price_grids_services/getPriceGridByProductIdAndLocationId'
  end

  resources :price_grids
  post 'price_grids/create_multiple'
  resources :price_rules
  post 'price_rules/create_multiple'

  resources :product_attributes
  resources :ad_management
  resources :custom_attributes
  resources :event_managements
  resources :price_grids
  #   root url for web app module
  root 'login#index'

end