require 'tzinfo'
# Load the Rails application.
# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
AdInWebApp::Application.initialize!
ALL_CATEGORIES_IN_HASH = Api::ProductServices.getAllCategoriesInHash
ALL_BANNERS_IN_HASH = Api::LocationServices.getAllBannersInHash
ALL_PRODUCT_ATTRIBUTE=Api::ProductServices.getAllProductAttributes
ALL_LOCATION_ATTRIBUTE=Api::LocationServices.getAllLocationAttributes
