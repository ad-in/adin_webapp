# require '../AdInServices/app/models/api/location_services.rb'

class PriceGridsController < ApplicationController
  respond_to :html, :json
  ActionController::Parameters.permit_all_parameters = true
  before_action :set_price_grid, only: [:show, :edit, :update, :destroy]
  # GET /price_grids
  # GET /price_grids.json
  def index
    @price_grids = PriceGrid.all
    @price_grid = PriceGrid.new
    
    logger.info { "finally in the logger and location =#{@locations}" }
  
  end

  # GET /price_grids/1
  # GET /price_grids/1.json
  def show
    @price_grid.destroy
    redirect_to pricing_guidelines_url
    logger.info { "finally in destroy method" }
  end

  # GET /price_grids/new
  def new
    @price_grid = PriceGrid.new
  end

  # GET /price_grids/1/edit
  def edit
  end

  # POST /price_grids
  # POST /price_grids.json
  def create
    @price_grid = PriceGrid.new

    respond_to do |format|
      if @price_grid.save
        format.html { render :index, notice: 'Price grid was successfully created.' }
        format.json { render :index, status: :created, location: @price_grid }
      else
        format.html { render :new }
        format.json { render json: @price_grid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /price_grids/1
  # PATCH/PUT /price_grids/1.json
  def update
    respond_to do |format|
      if @price_grid.update(price_grid_params)
        format.html { redirect_to @price_grid, notice: 'Price grid was successfully updated.' }
        format.json { render :show, status: :ok, location: @price_grid }
      else
        format.html { render :edit }
        format.json { render json: @price_grid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /price_grids/1
  # DELETE /price_grids/1.json
  def destroy
    @price_grid.destroy
    redirect_to pricing_guidelines_url
    logger.info { "finally in destroy method" }
    end
 
  # POST /price_grids
  # POST /price_grids.json
  def create_multiple
      params[:price_grid].each do |price_grid|
      price_grid=PriceGrid.create(price_grid)
      logger.info { "finally in the logger =#{price_grid}" }
    end
   redirect_to pricing_guidelines_url
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_price_grid
    @price_grid = PriceGrid.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def price_grid_params
    params.require(:price_grid).permit(:name, :description, :price_vehicle_id, :from_price, :to_price, :price_candidates, :multiples, :created_by, :updated_by)
  end

end
