require '../AdInServices/app/models/api/event_management_services.rb'
require '../AdInServices/app/models/event_management.rb'
require '../AdInServices/app/models/api/location_group_services.rb'
require '../AdInServices/app/models/api/product_group_services.rb'

class EventManagementsController < ApplicationController
  before_action :set_event_management, only: [:show, :edit, :update, :destroy]
 skip_before_action :verify_authenticity_token

  # GET /event_managements
  # GET /event_managements.json
  def index
   @event_managements = Api::EventManagementServices.getAllEventsWithTrailName()
    @events=@event_managements.map do |r|
      { :dates => [r["start_date"].to_datetime,r["end_date"].to_datetime],:title => r["name"],:description => r["description"]}
     end
    @events = @events.to_json 
  end

  # GET /event_managements/1
  # GET /event_managements/1.json
  def show
  end

  # GET /event_managements/new
  def new
   @event_management = EventManagement.new
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
  end

  # GET /event_managements/1/edit
  def edit
     @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
     @allBannersHierarchy = ALL_BANNERS_IN_HASH
     @catArray = Api::ProductServices.getCategoryIdsFromCategoryTrail(@event_management.product_scope_trail)
     @locationArray = Api::LocationServices.getLocationIdsFromLocationTrail(@event_management.location_scope_trail)
  end

  # POST /event_managements
  # POST /event_managements.json
  def create
    @event_management = EventManagement.new(event_management_params)
    
    eventMngt_Date=params[:eventMngt_Date]

    dates = eventMngt_Date.split("-")
    start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngt_Date].blank?
    end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngt_Date].blank?
    
    #start_date= Date.strptime(params[:eventMngtStartDate].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngtStartDate].blank?
    #end_date= Date.strptime(params[:eventMngtEndDate].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngtEndDate].blank?
    @event_management.start_date = start_date
    @event_management.end_date = end_date
    @event_management.product_scope_trail = Api::ProductGroupServices.getProductTrail(params[:selectedCatId]).to_json unless params[:selectedCatId].blank?
    @event_management.location_scope_trail = Api::LocationGroupServices.createLocationTrail(params[:selectedBannerId]).to_json unless params[:selectedBannerId].blank?

    respond_to do |format|
      if Api::EventManagementServices.addEvent(@event_management)
        format.html { redirect_to "/event_managements", notice: 'Event management was successfully created.' }
        format.json { render :show, status: :created, location: @event_management }
      else
        @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
        @allBannersHierarchy = ALL_BANNERS_IN_HASH
        format.html { render :new }
        format.json { render json: @event_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_managements/1
  # PATCH/PUT /event_managements/1.json
  def update
    eventMngt_Date=params[:eventMngt_Date]

    dates = eventMngt_Date.split("-")
    start_date= Date.strptime(dates[0].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngt_Date].blank?
    end_date= Date.strptime(dates[1].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngt_Date].blank?
    
    #start_date= Date.strptime(params[:eventMngtStartDate].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngtStartDate].blank?
    #end_date= Date.strptime(params[:eventMngtEndDate].gsub(" ",""), '%m/%d/%Y') unless params[:eventMngtEndDate].blank?
    @event_management.start_date = start_date
    @event_management.end_date = end_date
    @event_management.product_scope_trail = Api::ProductGroupServices.getProductTrail(params[:selectedCatId]).to_json unless params[:selectedCatId].blank?
    @event_management.location_scope_trail = Api::LocationGroupServices.createLocationTrail(params[:selectedBannerId]).to_json unless params[:selectedBannerId].blank?

    respond_to do |format|
      if Api::EventManagementServices.updateEvent(@event_management,event_management_params)
        format.html { redirect_to event_managements_url, notice: 'Event management was successfully updated.' }
        format.json { render :show, status: :ok, location: @event_management }
      else
        format.html { render :edit }
        format.json { render json: @event_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_managements/1
  # DELETE /event_managements/1.json
  def destroy
    respond_to do |format|
     if Api::EventManagementServices.removeEvent(@event_management)
    
      format.html { redirect_to event_managements_url, notice: 'Event management was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
end


  def newEvent
     @event_management = EventManagement.new
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
  end


  def addEvent
   
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_management
      @event_management = EventManagement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_management_params
      params.require(:event_management).permit(:name, :start_date, :end_date, :description, :created_by, :product_scope_trail, :location_scope_trail)
    end
end
