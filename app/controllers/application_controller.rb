require '../AdInServices/app/models/api/user_services.rb'
require '../AdInServices/app/models/api/location_services.rb'
require '../AdInServices/app/models/api/location_group_services.rb'
require '../AdInServices/app/models/api/product_group_services.rb'
require '../AdInServices/app/models/api/product_services.rb'
require '../AdInServices/app/models/user.rb'
require '../AdInServices/app/models/user_locations.rb'
require '../AdInServices/app/models/user_categories.rb'
require '../AdInServices/app/models/location_group.rb'
require '../AdInServices/app/models/product_group.rb'
require '../AdInServices/app/models/contract.rb'
require '../AdInServices/app/models/store.rb'
require '../AdInServices/app/models/location_hier.rb'
require '../AdInServices/app/models/tier.rb'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user
  before_action :current_contract_and_tier
  helper_method :current_user
  
  protected
  def authenticate_user
    if session[:user_id]
    # set current user object to @current_user object variable
    @current_user = User.find session[:user_id]
    return true 
    else
    	flash[:error] = "Please login first"
     redirect_to root_url
    return false
    end
  end
  
  def current_contract_and_tier
  	@current_contract = session[:contract].blank? ? Contract.last : Contract.find_by_contract_id(session[:contract]['contract_id']) 
  	@current_tier = session[:tier].blank? ? Tier.find_by_contract_id(@current_contract.contract_id) : Tier.find_by_tier_id(session[:tier]['tier_id']) 
  end

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
end
