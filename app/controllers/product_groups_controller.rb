class ProductGroupsController < ApplicationController
  before_action :set_product_group, only: [:show, :edit, :update, :destroy,:modify]
  skip_before_action :verify_authenticity_token
  
  def index
    @product_groups = Api::ProductGroupServices.getAllProductsGroupsInHash
  end

  def new
    @product_group = ProductGroup.new
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
  end

  def edit
  	@allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
  end
   def modify
  	@allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
  end

  def create
    @product_group = ProductGroup.new(product_group_params)
    respond_to do |format|
       if Api::ProductGroupServices.addProductGroup(@product_group, current_user, params)
        format.html { redirect_to "/product_groups", notice: 'Product group was successfully created.' }
        format.json { render :index, status: :created, location: @product_groups}
       else
         @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
         format.html { render action: 'new' }
         format.json { render json: @product_group.errors, status: :unprocessable_entity }
       end
    end
  end

  # PATCH/PUT /location_groups/1
  # PATCH/PUT /location_groups/1.json
  def update
    respond_to do |format|
      if Api::ProductGroupServices.updateProductGroup(@product_group, current_user, product_group_params ,params)
        format.html { redirect_to "/product_groups", notice: 'Product group was successfully updated.' }
        format.json { head :no_content }
      else
        @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
        format.html { render action: 'edit' }
        format.json { render json: @product_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /location_groups/1
  # DELETE /location_groups/1.json
  def destroy
    respond_to do |format|
      if Api::ProductGroupServices.removeProductGroup(@product_group)
      format.html { redirect_to "/product_groups" }
      format.json { head :no_content }
      end
    end
  end

  def removeMultipleGroups
    logger.info"++++in product_groups controller action and ids = #{params[:groupIds]}"
    groupIds = params[:groupIds]
    Api::ProductGroupServices.removeBulkProductGroup(groupIds)  
    # respond_to do |format|
    #   format.json
    #   format.js
    # end
  end

  def getProducts
    @products = Api::ProductServices.getProductsByCategory(params[:category])
    respond_to do |format|
      format.json { render json: @products, status: :ok}
      format.js
    end
  end  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_group
      @product_group = ProductGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_group_params
      params.require(:product_group).permit(:name, :description, :product_trail, :product_attribute, :created_by, :updated_by)
    end
end
