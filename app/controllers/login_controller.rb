class LoginController < ApplicationController
 skip_before_action :authenticate_user, :current_contract_and_tier
  skip_before_action :verify_authenticity_token

  layout :false
  def index
  end
  
  def create
    user = Api::UserServices.authenticate(params[:username], params[:password])
    if user 
      session[:user_id]=user.id
      user.update_attribute(:last_login_time, Time.zone.now)
       flash[:notice] = "Welcome to AdInWebapp, you logged in as #{user.username}"
      redirect_to "/worklist/index"
    else
      flash[:error] = 'Invalid email/password combination'
      render 'index'
    end
  end
 

  def logout
   session[:user_id]= nil
   session[:contract_id] = nil
   session[:tier_id] = nil
   flash[:notice] = "Logout successfully"
   redirect_to root_url, :notice => "Logged out!"
  end
end
