require '../AdInServices/app/models/api/product_services.rb'

class PriceRulesController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :html, :json
  before_action :set_price_rule, only: [:show, :edit, :update, :destroy]
  ActionController::Parameters.permit_all_parameters = true
  # GET /price_rules
  # GET /price_rules.json
  def index
    @price_rules = PriceRule.all
    @location =Api::LocationServices.getAllBanner
    @allLocationsHierarchy = Array.new
    @location.each{|banner|
      l = Api::LocationServices.getAllSubBanners(banner)
      @allLocationsHierarchy.push(l)
    }
    @allCategoriesHierarchy = Api::ProductServices.getAllCategoriesInHash
    category_id=params[:category_id]
    if (category_id !=nil)
      @price_rules = PriceRule.where("category_id=#{category_id}")
      respond_to do |format|
        format.json { render json: @price_rules, status: :ok }
        format.js
        logger.info { "finally in the logger =#{@response}" }
      end
    else
      @price_rules = PriceRule.all
    end

  end

  # GET /price_rules/1

  # GET /price_rules/1.json
  def show
  end

  # GET /price_rules/new

  def new
    @price_rule = PriceRule.new
  end

  # GET /price_rules/1/edit

  def edit
  end

  # POST /price_rules

  # POST /price_rules.json
  def create
    @price_rule = PriceRule.new(price_rule_params)

    respond_to do |format|
      if @price_rule.save
        format.html { redirect_to @price_rule, notice: 'Price rule was successfully created.' }
        format.json { render action: 'show', status: :created, location: @price_rule }
      else
        format.html { render action: 'new' }
        format.json { render json: @price_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /price_rules/1

  # PATCH/PUT /price_rules/1.json
  def update
    respond_to do |format|
      if @price_rule.update(price_rule_params)
        format.html { redirect_to @price_rule, notice: 'Price rule was successfully updated.' }
        format.json { respond_with_bip(@price_rule) }
      else
        format.html { render action: 'edit' }
        format.json { respond_with_bip(@price_rule) }
      end
    end
  end

  # DELETE /price_rules/1

  # DELETE /price_rules/1.json
  def destroy
    @price_rule.destroy
    respond_to do |format|
      format.html { redirect_to price_rules_url }
      format.json { head :no_content }
    end
  end

  # POST /price_rules

  # POST /price_rules.json
  def create_multiple
    params[:price_rule].each do |price_rule|
      price_rule=PriceRule.create(price_rule)
      logger.info { "finally in the logger =#{price_rule}" }
    end
    redirect_to price_rules_url
  end

  # POST /price_rules
  # POST /price_rules.json

  def get_rules
    category_id=params[:category_id]
    if (category_id !=nil)
      @price_rules = PriceRule.where("category_id=#{category_id}")
      respond_to do |format|
        format.json { render json: @price_rules, status: :ok }
        format.js
        logger.info { "finally in the logger =#{@response}" }
      end
    else
      @price_rules = PriceRule.all
      format.json { render json: @price_rules, status: :ok }
      format.js
      logger.info { "finally in the logger =#{@response}" }
    end
  end
  private

  # Use callbacks to share common setup or constraints between actions.
  def set_price_rule
    @price_rule = PriceRule.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def price_rule_params
    params.require(:price_rule).permit(:name, :description, :marketing_vehicle_id, :location_id, :min_margin, :max_margin, :max_passthrough, :optimization_goal, :category_id, :min_passthrough, :min_price_change, :max_price_change)
  end
end
