require '../AdInServices/app/models/api/contract_services.rb'
require '../AdInServices/app/models/contract.rb'
require '../AdInServices/app/models/version.rb'
require '../AdInServices/app/models/tier_product.rb'
require '../AdInServices/app/models/tier.rb'
require '../AdInServices/app/models/store.rb'
require '../AdInServices/app/models/product.rb'
require '../AdInServices/app/models/user.rb'
require '../AdInServices/app/models/location_hier.rb'
require '../AdInServices/app/models/custom_attribute.rb'
class WorklistController < ApplicationController
  def index
     @contracts = Api::ContractServices.getRecordByParams(params)
    # @contracts = Contract.get_record(params)
    @allCategoriesHierarchy = ALL_CATEGORIES_IN_HASH
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
    @custom_location_attributes = Api::LocationServices.getAllCustomAttr

  end
end
