require '../AdInServices/app/models/api/calendar_services.rb'
require '../AdInServices/app/models/api/event_management_services.rb'

class AdManagementController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def index
     @res = Api::CalendarServices.getAllWeek
    # @sections = {:dates => [@res['ad_wk_start_dt'].to_datetime,@res['ad_wk_end_dt'].to_datetime]}
   
     @eventsResultset = Api::EventManagementServices.getEvents(@res['start_date'] ,@res['end_date'])
     
     @events=@eventsResultset.map do |r|
      { :dates => [r["start_date"].to_datetime,r["end_date"].to_datetime],:title => r["name"],:description => r["description"]+"<br>Start date: "+r["start_date"]+"<br>End Date: "+r["end_date"]}
     end
    @events = @events.to_json
     # puts @json  
     puts @events
  end

end
