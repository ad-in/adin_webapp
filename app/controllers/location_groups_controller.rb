require '../AdInServices/app/models/store.rb'
require '../AdInServices/app/models/location_hier.rb'
require '../AdInServices/app/models/custom_attribute.rb'
# require '../AdInServices/app/models/api/location_group_services.rb'
class LocationGroupsController < ApplicationController
  before_action :set_location_group, only: [:show, :edit, :update, :destroy,:modify]
  skip_before_action :verify_authenticity_token

  def index
   @location_groups = Api::LocationGroupServices.getAllLocationGroupsInHash
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
    @custom_location_attributes = Api::LocationServices.getAllCustomAttr
  end

  def show
  end

  # GET /location_groups/new
  def new
    @location_group = LocationGroup.new
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
    @custom_location_attributes = Api::LocationServices.getAllCustomAttr
  end

  # GET /location_groups/1/edit
  def edit
  end
  def modify
    @allBannersHierarchy = ALL_BANNERS_IN_HASH
    @custom_location_attributes = Api::LocationServices.getAllCustomAttr
  end
  # POST /location_groups
  # POST /location_groups.json
  def create
    @location_group = LocationGroup.new(location_group_params)
    respond_to do |format|
       if Api::LocationGroupServices.addLocationGroup(@location_group, current_user, params)
        format.html { redirect_to "/location_groups", notice: 'Location group was successfully created.' }
        format.json { render :index, status: :created, location: @location_groups}
       else
         @allBannersHierarchy = ALL_BANNERS_IN_HASH
         @custom_location_attributes = Api::LocationServices.getAllCustomAttr
         format.html { render action: 'new' }
         format.json { render json: @location_group.errors, status: :unprocessable_entity }
       end
    end
  end

  # PATCH/PUT /location_groups/1
  # PATCH/PUT /location_groups/1.json
  def update
    respond_to do |format|
      if Api::LocationGroupServices.updateLocationGroup(@location_group, current_user, location_group_params ,params)
        format.html { redirect_to "/location_groups", notice: 'Location group was successfully updated.' }
        format.json { head :no_content }
      else
        @allBannersHierarchy = ALL_BANNERS_IN_HASH
        @custom_location_attributes = Api::LocationServices.getAllCustomAttr
        format.html { render action: 'modify' }
        format.json { render json: @location_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /location_groups/1
  # DELETE /location_groups/1.json
  def destroy
    respond_to do |format|
      if Api::LocationGroupServices.removeLocationGroup(@location_group)
      format.html { redirect_to "/location_groups" }
      format.json { head :no_content }
      end
    end
  end

  def getStores
    @stores = Api::LocationServices.getStoresByBanner(params[:banner])
    respond_to do |format|
      format.json { render json: @stores, status: :ok}
      format.js
    end
  end
  def getStoresByCustAttr
    @stores = Api::LocationServices.getStoresByCustomAttr(params[:custattrid], params[:custattrval])
    respond_to do |format|
      format.json { render json: @stores, status: :ok}
      format.js
    end
  end
  def removeMultipleGroups
    logger.info"++++in location_groups controller action and ids = #{params[:groupIds]}"
    groupIds = params[:groupIds]
    Api::LocationGroupServices.removeBulkLocationGroup(groupIds)  
    # respond_to do |format|
    #   format.json
    #   format.js
    # end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location_group
      @location_group = LocationGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_group_params
      params.require(:location_group).permit(:name, :description, :location_trail, :location_attribute, :created_by, :updated_by)
    end
end
