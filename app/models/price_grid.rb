class PriceGrid < ActiveRecord::Base
  validates :price_vehicle_id, presence: true
  validates :from_price, presence: true, if: :price_vehicle?
  validates :to_price, presence: true, if: :price_vehicle?
  validates :multiples, presence: true, if: :price_vehicle_multiples?
  
  def price_vehicle?
    price_vehicle_id !=1
  end
  def price_vehicle_multiples?
    price_vehicle_id==1
  end
  
   def self.get_price_grids(category_id,location_id)

    logger.info { "category_id2 =#{category_id}" }
    logger.info { "location_id2 =#{location_id}" }
    if (!category_id.blank? && !location_id.blank? )
      @price_grids = PriceGrid.where("location_id=#{location_id}  AND category_id=#{category_id} ")

    else if(!category_id.blank? && location_id.blank?)

        @price_grids = PriceGrid.where("category_id=#{category_id} and location_id IS NULL")

      else if(category_id.blank? && !location_id.blank?)
          @price_grids = PriceGrid.where("location_id=#{location_id} and category_id IS NULL")

        else
          @price_grids = nil

        end
      end
    end
    logger.info { "@price_grids =#{@price_grids}" }
    return @price_grids

  end
  
  
end
