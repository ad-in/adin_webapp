class PriceRule < ActiveRecord::Base
  #validates :min_margin, presence: true
  #validates :min_price_change, presence: true
  #validates :max_price_change, presence: true
  #validates :min_passthrough, presence: true
  #validates :max_passthrough, presence: true
  #validates :optimization_goal, presence: true
  #validates :marketing_vehicle_id, presence: true
  def self.get_price_rules(category_id,location_id)

    logger.info { "category_id2 =#{category_id}" }
    logger.info { "location_id2 =#{location_id}" }
    if (!category_id.blank? && !location_id.blank? )
      @price_rules = PriceRule.where("location_id=#{location_id}  AND category_id=#{category_id} ")

    else if(!category_id.blank? && location_id.blank?)

        @price_rules = PriceRule.where("category_id=#{category_id} and location_id IS NULL")

      else if(category_id.blank? && !location_id.blank?)
          @price_rules = PriceRule.where("location_id=#{location_id} and category_id IS NULL")

        else
          @price_rules = nil

        end
      end
    end
    logger.info { "@price_rules =#{@price_rules}" }
    return @price_rules

  end

end
