json.array!(@price_rules) do |price_rule|
  json.extract! price_rule, :id, :name, :varying, :description, :marketing_vehicle_id, :banner_id, :min_margin, :max_margin, :max_passthrough, :optimization_goal, :varying, :category_id, :min_passthrough, :min_price_change, :max_price_change
  json.url price_rule_url(price_rule, format: :json)
end
