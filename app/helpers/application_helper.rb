module ApplicationHelper
  def page_title
    if controller_name == "price_grids"
      title = "Pricing Guidelines"
    else
      title = controller_name.humanize
    end
     title = "Pricing Promotion |"+" "+ title
  end

  def tab_selection(name)
    result = nil
    if name == 'worklist'
      result = ['worklist'].include? controller_name
    else 
      result = ['contract'].include? controller_name
    end
    if result 
      result = "active"
    end
    result
  end

  def formatted_html
    content_tag(:ul ,
      content_tag(:li),
     id: 'productHeirarchyTree')
  end
  
  def hash_list_tag(hash)
    html = content_tag(:ul) {
    ul_contents = ""
    ul_contents << content_tag(:li, hash[:parent])
    hash[:children].each do |child|
      ul_contents << hash_list_tag(child)
    end

    ul_contents.html_safe
  }.html_safe
  end
  
  # def ihash(h)
    # html = content_tag(:ul){ 
      # ul_contents =""
      # h.each do |k,v|
        # ul_contents << content_tag(:li , k)
        # if v.is_a?(Hash)
          # ul_contents << content_tag(:li ,
          # ihash(v)
          # )
        # else
          # ul_contents << content_tag(:ul ,
          # content_tag(:li, k ,class: "treeLastChild"),
          # )
        # end
      # end
      # ul_contents.html_safe
#       
      # }.html_safe
    # end
end
