module ContractHelper
	# helper for contract status image
	def contract_status_img(status)
		if status == "approved"
			 image_tag 'Approved.png'
	    elsif status == "rejected"
	         image_tag 'Rejected.png'
	    elsif status == "optimized"
	     	image_tag 'Optimized.png'
	    else
			image_tag 'modify_contract_icon.png', :hight=> "10px", :width=>"10px"
	    end
	end

end
