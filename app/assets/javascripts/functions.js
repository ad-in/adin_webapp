// JavaScript Document

function listbox_move(listID, direction) {

	var listbox = document.getElementById(listID);
	var selIndex = listbox.selectedIndex;

	if (-1 == selIndex) {
		alert("Please select an option to move.");
		return;
	}

	var increment = -1;
	if (direction == 'up')
		increment = -1;
	else
		increment = 1;

	if ((selIndex + increment) < 0 || (selIndex + increment) > (listbox.options.length - 1)) {
		return;
	}

	var selValue = listbox.options[selIndex].value;
	var selText = listbox.options[selIndex].text;
	listbox.options[selIndex].value = listbox.options[selIndex + increment].value
	listbox.options[selIndex].text = listbox.options[selIndex + increment].text

	listbox.options[selIndex + increment].value = selValue;
	listbox.options[selIndex + increment].text = selText;

	listbox.selectedIndex = selIndex + increment;
}

function listbox_moveacross(sourceID, destID) {
	var src = document.getElementById(sourceID);
	var dest = document.getElementById(destID);

	for (var count = 0; count < src.options.length; count++) {

		if (src.options[count].selected == true) {
			var option = src.options[count];

			var newOption = document.createElement("option");
			newOption.value = option.value;
			newOption.text = option.text;
			newOption.selected = true;
			try {
				dest.add(newOption, null);
				//Standard
				src.remove(count, null);
			} catch(error) {
				dest.add(newOption);
				// IE only
				src.remove(count);
			}
			count--;
		}
	}
}

/*+ Function to show/hide popup overlay +*/
function handleOverlayTry(overlay, action, hiddenId) {

	/* hide all existing popups */
	if (document.getElementById("worklistCustomizeTable")) {
		document.getElementById("worklistCustomizeTable").style.display = "none";
		document.getElementById("worklistResetFilter").style.display = "none";
	}

	if (action == "show") {

		for (var i = 0; i < 10; i++) {
			document.getElementsByName('dollar1')[i].checked = false;
			document.getElementsByName('dollar2')[i].checked = false;
			document.getElementsByName('cents1')[i].checked = false;
			document.getElementsByName('cents2')[i].checked = false;
		}

		//var demo = $("#hiddensetValuesOverlayId").val();

		test = $('#' + hiddenId + ' #price_grid__price_candidates').val();
		from_price = $('#' + hiddenId + ' #price_grid__from_price').val();
		to_price = $('#' + hiddenId + ' #price_grid__to_price').val();

		if (test != "") {
			var arr = test.split(',');
			for (var i = 0; i < arr.length; i++) {

				var number = arr[i].toString().trim();

				document.getElementsByName('dollar1')[number[0]].checked = true;
				document.getElementsByName('dollar2')[number[1]].checked = true;
				document.getElementsByName('cents1')[number[3]].checked = true;
				document.getElementsByName('cents2')[number[4]].checked = true;

			}
		}
		document.getElementById(overlay).style.display = "inline";
		document.getElementById("hidden" + overlay + "Id").value = hiddenId;
	} else if (action == "hide") {

		document.getElementById(overlay).style.display = "none";
	}
}//handleOverlay  ends

/*+ Function to show/hide popup overlay +*/
function handleOverlay(overlay, action) {
	/* hide all existing popups */
	if (document.getElementById("worklistCustomizeTable")) {
		document.getElementById("worklistCustomizeTable").style.display = "none";
		document.getElementById("worklistResetFilter").style.display = "none";
	}

	if (action == "show") {
		document.getElementById(overlay).style.display = "inline";

	} else if (action == "hide") {
		document.getElementById(overlay).style.display = "none";
	}
}//handleOverlay  ends

/*+ Function to show/hide price rule and price grid popup overlay +*/
function handleOverlayPricingGuidelines(overlay, action, id) {
	/* hide all existing popups */
	if (document.getElementById("worklistCustomizeTable")) {
		document.getElementById("worklistCustomizeTable").style.display = "none";
		document.getElementById("worklistResetFilter").style.display = "none";
	}

	if (action == "show") {
		document.getElementById(overlay).style.display = "inline";

	} else if (action == "hide") {
		document.getElementById(overlay).style.display = "none";
	}
}//handleOverlay  ends

/* function to remove filter */
function removeFilter(el) {

	// while there are parents, keep going until reach TR
	while (el.parentNode && el.tagName.toLowerCase() != 'tr') {
		el = el.parentNode;
	}

	// If el has a parentNode it must be a TR, so delete it
	// Don't delte if only 3 rows left in table
	if (el.parentNode && el.parentNode.rows.length > 2) {
		el.parentNode.removeChild(el);
	}
}

var idForAddbtn = 4;
/* function to add filter */
function addFilterTry(tableID, para,price_vehicle_id) {

	var table = document.getElementById(tableID);

	if (!table)
		return;

	//alert(table.rows[0].className);

	/* If table has TH element, then clone next TR content*/
	var temp = '<tr id="' + idForAddbtn + '"><input id="price_grid__price_vehicle_id" name="price_grid[][price_vehicle_id]" type="hidden" value="'+price_vehicle_id+'"> <input id="price_grid__category_id" name="price_grid[][category_id]" type="hidden" > <input id="price_grid__location_id" name="price_grid[][location_id]" type="hidden" > <input id="price_grid__product_attribute" name="price_grid[][product_attribute]" type="hidden" value="0"> <input id="price_grid__location_attribute" name="price_grid[][location_attribute]" type="hidden" value="0"> <td> <input id="price_grid__from_price" name="price_grid[][from_price]" type="text"> </td>	<td> <input id="price_grid__to_price" name="price_grid[][to_price]" type="text"> </td> ' + '<td> <input id="price_grid__price_candidates" name="price_grid[][price_candidates]" type="text"> </td>' + '	<td><a href="javascript:;" onclick="handleOverlayTry(\'setValuesOverlay\',\'show\',' + idForAddbtn + ');">Set Values</a></td> <td></td> <td></td>' + '	<td><a href="javascript:;" onclick="removeFilter(this);"> Remove </a></td>	</tr>';
	idForAddbtn++;
	$('#' + tableID + ' tr:last').before(temp);
}

/* function to add filter */
function addFilter(tableID, para) {

	var table = document.getElementById(tableID);

	if (!table)
		return;

	//alert(table.rows[0].className);

	/* If table has TH element, then clone next TR content*/

	if (table.rows[0].className == "tableHeadingTH") {
		var newRow = table.rows[1].cloneNode(true);
	} else {
		var newRow = table.rows[0].cloneNode(true);
	}

	// Now get the inputs and modify their names
	var inputs = newRow.getElementsByTagName('TD');

	for (var i = 0, iLen = inputs.length; i < iLen; i++) {
		// Update inputs[i]
	}

	var addBtn = document.getElementById(para);

	// Add the new row to the tBody (required for IE)

	var tBody = table.tBodies[0];
	tBody.insertBefore(newRow, addBtn);
}