require "rails_helper"

RSpec.describe PriceRulesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/price_rules").to route_to("price_rules#index")
    end

    it "routes to #new" do
      expect(:get => "/price_rules/new").to route_to("price_rules#new")
    end

    it "routes to #show" do
      expect(:get => "/price_rules/1").to route_to("price_rules#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/price_rules/1/edit").to route_to("price_rules#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/price_rules").to route_to("price_rules#create")
    end

    it "routes to #update" do
      expect(:put => "/price_rules/1").to route_to("price_rules#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/price_rules/1").to route_to("price_rules#destroy", :id => "1")
    end

  end
end
