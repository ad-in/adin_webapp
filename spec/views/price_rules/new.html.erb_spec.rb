require 'rails_helper'

RSpec.describe "price_rules/new", :type => :view do
  before(:each) do
    assign(:price_rule, PriceRule.new(
      :name => "",
      :varying => "MyString",
      :description => "MyText",
      :marketing_vehicle_id => 1,
      :banner_id => 1,
      :min_margin => 1,
      :max_margin => 1,
      :max_passthrough => 1,
      :optimization_goal => "",
      :varying => "MyString",
      :category_id => "",
      :min_passthrough => 1,
      :min_price_change => 1,
      :max_price_change => 1
    ))
  end

  it "renders new price_rule form" do
    render

    assert_select "form[action=?][method=?]", price_rules_path, "post" do

      assert_select "input#price_rule_name[name=?]", "price_rule[name]"

      assert_select "input#price_rule_varying[name=?]", "price_rule[varying]"

      assert_select "textarea#price_rule_description[name=?]", "price_rule[description]"

      assert_select "input#price_rule_marketing_vehicle_id[name=?]", "price_rule[marketing_vehicle_id]"

      assert_select "input#price_rule_banner_id[name=?]", "price_rule[banner_id]"

      assert_select "input#price_rule_min_margin[name=?]", "price_rule[min_margin]"

      assert_select "input#price_rule_max_margin[name=?]", "price_rule[max_margin]"

      assert_select "input#price_rule_max_passthrough[name=?]", "price_rule[max_passthrough]"

      assert_select "input#price_rule_optimization_goal[name=?]", "price_rule[optimization_goal]"

      assert_select "input#price_rule_varying[name=?]", "price_rule[varying]"

      assert_select "input#price_rule_category_id[name=?]", "price_rule[category_id]"

      assert_select "input#price_rule_min_passthrough[name=?]", "price_rule[min_passthrough]"

      assert_select "input#price_rule_min_price_change[name=?]", "price_rule[min_price_change]"

      assert_select "input#price_rule_max_price_change[name=?]", "price_rule[max_price_change]"
    end
  end
end
