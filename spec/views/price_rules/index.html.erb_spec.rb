require 'rails_helper'

RSpec.describe "price_rules/index", :type => :view do
  before(:each) do
    assign(:price_rules, [
      PriceRule.create!(
        :name => "",
        :varying => "Varying",
        :description => "MyText",
        :marketing_vehicle_id => 1,
        :banner_id => 2,
        :min_margin => 3,
        :max_margin => 4,
        :max_passthrough => 5,
        :optimization_goal => "",
        :varying => "Varying",
        :category_id => "",
        :min_passthrough => 6,
        :min_price_change => 7,
        :max_price_change => 8
      ),
      PriceRule.create!(
        :name => "",
        :varying => "Varying",
        :description => "MyText",
        :marketing_vehicle_id => 1,
        :banner_id => 2,
        :min_margin => 3,
        :max_margin => 4,
        :max_passthrough => 5,
        :optimization_goal => "",
        :varying => "Varying",
        :category_id => "",
        :min_passthrough => 6,
        :min_price_change => 7,
        :max_price_change => 8
      )
    ])
  end

  it "renders a list of price_rules" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Varying".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Varying".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
  end
end
