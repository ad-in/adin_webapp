require 'rails_helper'

RSpec.describe "price_rules/show", :type => :view do
  before(:each) do
    @price_rule = assign(:price_rule, PriceRule.create!(
      :name => "",
      :varying => "Varying",
      :description => "MyText",
      :marketing_vehicle_id => 1,
      :banner_id => 2,
      :min_margin => 3,
      :max_margin => 4,
      :max_passthrough => 5,
      :optimization_goal => "",
      :varying => "Varying",
      :category_id => "",
      :min_passthrough => 6,
      :min_price_change => 7,
      :max_price_change => 8
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Varying/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Varying/)
    expect(rendered).to match(//)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/7/)
    expect(rendered).to match(/8/)
  end
end
