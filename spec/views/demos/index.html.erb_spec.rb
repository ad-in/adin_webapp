require 'rails_helper'

RSpec.describe "demos/index", :type => :view do
  before(:each) do
    assign(:demos, [
      Demo.create!(
        :price => ""
      ),
      Demo.create!(
        :price => ""
      )
    ])
  end

  it "renders a list of demos" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
